package io.everon;

import io.everon.connections.ConnectionService;

import java.util.List;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        var cs = new ConnectionService();
        List<Partner> effectiveConnnections = cs.findConnectionForPartner(ConnectionService.CPO_AT_EVERON);
        System.out.println(effectiveConnnections);
    }
}
