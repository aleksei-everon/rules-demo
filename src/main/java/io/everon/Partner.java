package io.everon;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Partner {
    String id;
    Country country;
    Type type;
    boolean isManagedByEveron;
    Protocol protocol;

    @SuppressWarnings("unused")
    public enum Country {
        NL,
        DE,
        AT
    }

    @SuppressWarnings("unused")
    public enum Type {
        CPO,
        MSP
    }

    public enum Protocol {
        OCPI,
        OICP
    }
}
