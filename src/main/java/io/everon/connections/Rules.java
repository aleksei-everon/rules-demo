package io.everon.connections;

import com.deliveredtechnologies.rulebook.NameValueReferableTypeConvertibleMap;
import com.deliveredtechnologies.rulebook.Result;
import com.deliveredtechnologies.rulebook.lang.RuleBuilder;
import com.deliveredtechnologies.rulebook.model.Rule;
import io.everon.Partner;

import java.util.function.BiConsumer;
import java.util.function.Predicate;

/**
 * Determines the effecitve connections of a provided partner.
 */
public class Rules {
    private Rules() {
    }

    static Rule<Partner, Boolean> partnerDoesnShareDataWithItself(Partner givenPartner) {
        return succeedWhen(
                candidate -> !candidate.equals(givenPartner),
                "A partner is not allowed to share data with itself");
    }

    static Rule<Partner, Boolean> partnersMustBeOfTheSameCountry(Partner givenPartner) {
        return succeedWhen(
                candidate -> givenPartner.getCountry().equals(candidate.getCountry()),
                "Partners must be of the same country"
        );
    }

    static Rule<Partner, Boolean> externalPartnersDontShareDataWithEachOther(Partner givenPartner) {
        return succeedWhen(
                candidate -> candidate.isManagedByEveron() || givenPartner.isManagedByEveron(),
                "External partners are not allowed to exchange data"
        );
    }

    static Rule<Partner, Boolean> internalPartnersMustHaveAContractWithAPartnerToShareData(Partner givenPartner) {
        return succeedWhen(
                // TODO here's what we would have to add a fact claiming whether the given partner has a contract with the candidate
                // TODO ommitted for simplicity
                candidate -> givenPartner.isManagedByEveron() && true,
                "The everon tenant must have a contract with the given partner"
        );

    }

    static Rule<Partner, Boolean> cpoSharesDataWithMspsOrViceVersa(Partner givenPartner) {
        return succeedWhen(
                candidate -> givenPartner.getType() != candidate.getType(),
                "It's not allowed for the partners of the same type to share data"
        );
    }

    // TODO of course this is not a real rule, it's here just for illustrative purposes
    static Rule<Partner, Boolean> oicpPartnersCantShareDataWithOCPIPartners(Partner givenPartner) {
        return succeedWhen(
                candidate -> givenPartner.getProtocol() != Partner.Protocol.OICP
                        || candidate.getProtocol() != Partner.Protocol.OCPI,
                "It's not allowed for the OICP partners to connect with the OCPI partners"
        );
    }

    static Rule<Partner, Boolean> succeedWhen(Predicate<Partner> predicate, String violationMessage) {
        return RuleBuilder.create()
                .withFactType(Partner.class)
                .withResultType(Boolean.class)
                .when(facts -> !predicate.test(facts.getOne()))
                .then(reportViolatedRule(violationMessage)).stop()
                .build();

    }

    static BiConsumer<NameValueReferableTypeConvertibleMap<Partner>, Result<Boolean>> reportViolatedRule(String message) {
        return (facts, result) -> {
            System.out.println("No. " + message);
            result.setValue(false);
        };
    }
}
