package io.everon.connections;

import com.deliveredtechnologies.rulebook.Fact;
import com.deliveredtechnologies.rulebook.FactMap;
import com.deliveredtechnologies.rulebook.Result;
import com.deliveredtechnologies.rulebook.lang.RuleBookBuilder;
import com.deliveredtechnologies.rulebook.model.RuleBook;
import io.everon.Partner;

import java.util.List;
import java.util.stream.Collectors;

public class ConnectionService {
    public static final Partner CPO_AT_EVERON = Partner.builder()
            .id("1")
            .isManagedByEveron(true)
            .type(Partner.Type.CPO)
            .country(Partner.Country.NL)
            .protocol(Partner.Protocol.OCPI)
            .build();

    public static final Partner EXTERNAL_HUBJECT_MSP = Partner.builder()
            .id("2")
            .isManagedByEveron(false)
            .type(Partner.Type.MSP)
            .country(Partner.Country.DE)
            .protocol(Partner.Protocol.OICP)
            .build();

    public static final Partner MSP_AT_EVERON = Partner.builder()
            .id("3")
            .isManagedByEveron(true)
            .type(Partner.Type.MSP)
            .country(Partner.Country.NL)
            .protocol(Partner.Protocol.OCPI)
            .build();

    public static final Partner EXTERNAL_OCPI_MSP_IN_NL = Partner.builder()
            .id("4")
            .isManagedByEveron(false)
            .type(Partner.Type.MSP)
            .country(Partner.Country.NL)
            .protocol(Partner.Protocol.OCPI)
            .build();

    private static List<Partner> allPartners() {
        return List.of(
                CPO_AT_EVERON,
                EXTERNAL_HUBJECT_MSP,
                MSP_AT_EVERON,
                EXTERNAL_OCPI_MSP_IN_NL
        );
    }

    public List<Partner> findConnectionForPartner(Partner givenPartner) {
        return allPartners().stream()
                .filter(candiate -> areAllowedToConnect(givenPartner, candiate))
                .collect(Collectors.toList());
    }

    private boolean areAllowedToConnect(Partner givenConnection, Partner candidate) {
        System.out.println("====== Do we allow " + givenConnection + " to exchange data with " + candidate + "? ======");
        var ruleBook = newRuleBookFor(givenConnection);
        var facts = new FactMap<>(new Fact<>(candidate));
        ruleBook.run(facts);
        boolean allowed = ruleBook.getResult().map(Result::getValue).orElse(false);
        if (allowed) {
            System.out.println("Yes! Such a connection is allowed.");
        }
        return allowed;
    }

    static RuleBook<Boolean> newRuleBookFor(Partner givenPartner) {
        return RuleBookBuilder.create()
                .withResultType(Boolean.class)
                .withDefaultResult(true)
                .addRule(Rules.partnerDoesnShareDataWithItself(givenPartner))
                .addRule(Rules.cpoSharesDataWithMspsOrViceVersa(givenPartner))
                .addRule(Rules.externalPartnersDontShareDataWithEachOther(givenPartner))
                .addRule(Rules.internalPartnersMustHaveAContractWithAPartnerToShareData(givenPartner))
                .addRule(Rules.partnersMustBeOfTheSameCountry(givenPartner))
                .addRule(Rules.oicpPartnersCantShareDataWithOCPIPartners(givenPartner))
                .build();
    }
}
