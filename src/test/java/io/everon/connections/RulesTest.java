package io.everon.connections;

import com.deliveredtechnologies.rulebook.Fact;
import com.deliveredtechnologies.rulebook.FactMap;
import com.deliveredtechnologies.rulebook.model.Rule;
import io.everon.Partner;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RulesTest {

    public static final Partner SOME_MANAGED_MSP_IN_NL = Partner.builder()
            .id("1")
            .isManagedByEveron(true)
            .type(Partner.Type.MSP)
            .country(Partner.Country.NL)
            .protocol(Partner.Protocol.OCPI)
            .build();

    public static final Partner SOME_MANAGED_CPO_IN_NL = Partner.builder()
            .id("2")
            .isManagedByEveron(true)
            .type(Partner.Type.CPO)
            .country(Partner.Country.NL)
            .protocol(Partner.Protocol.OCPI)
            .build();

    @Test
    public void testPartnerDoesnShareDataWithItselfRule() {
        assertRuleViolates(Rules.partnerDoesnShareDataWithItself(SOME_MANAGED_MSP_IN_NL), SOME_MANAGED_MSP_IN_NL);
        assertRuleHolds(Rules.partnerDoesnShareDataWithItself(SOME_MANAGED_MSP_IN_NL), SOME_MANAGED_CPO_IN_NL);
    }

    public void assertRuleViolates(Rule<Partner, Boolean> rule, Partner fact) {
        assertTrue(checkRuleViolates(rule, fact));
    }

    public void assertRuleHolds(Rule<Partner, Boolean> rule, Partner fact) {
        assertFalse(checkRuleViolates(rule, fact));
    }

    private boolean checkRuleViolates(Rule<Partner, Boolean> rule, Partner fact) {
        return rule.invoke(new FactMap<>(new Fact<>(fact)));
    }
}
